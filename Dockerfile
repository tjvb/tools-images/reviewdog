FROM alpine

ARG REVIEWDOGVERSION

RUN apk add git

RUN wget -O - -q https://raw.githubusercontent.com/reviewdog/reviewdog/master/install.sh | sh -s $REVIEWDOGVERSION
